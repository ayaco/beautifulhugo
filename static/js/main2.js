document.addEventListener('DOMContentLoaded', function(){

	/* ayaco:#12:URLハッシュによる表示開始位置がnavbarで隠れる */
	$(window).on("load hashchange", function(e){
		if(("" == window.location.hash) || (0 == $(".navbar").length)) return;
		const navbarH = $(".navbar").height();
		const adjustT = $(window.location.hash.replace(":", "\\:"))[0].getBoundingClientRect().top - navbarH;
		if(0 > adjustT) $("html,body").animate({scrollTop:("-=" + Math.abs(adjustT))}, 300);
	});
	
});
